package com.mycompany.practicacafeteria;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.CubicCurve;


public class PrimaryController implements Initializable{

    static String ecombo;
    
    @FXML
    private TableView<Cafe> tb1Cafes;

    @FXML
    private TableColumn columnaTipo;
    
    @FXML
    private TableColumn columnaCantidad;

    @FXML
    private TableColumn columnaTamaño;

    @FXML
    private TableColumn columnaPrecio;

    @FXML
    private RadioButton radioCapuccino;

    @FXML
    private RadioButton radioLatte;

    @FXML
    private RadioButton radioCortado;

    @FXML
    private TextField txtCantidad;

    @FXML
    private Button btnPedir;

    @FXML
    private TextField txtRecargar;

    @FXML
    private Button btnRecargar;
    
    @FXML
    private Label saldoActual;
    
    @FXML
    private Button btnEliminar;
    
    @FXML
    private TextField txtBuscar;
        
    @FXML
    private ComboBox<String> cmb1;
    
    @FXML
    private CubicCurve curva;
    
    @FXML
    private ColorPicker btnColor;
    
    @FXML
    private Button btnDefault;
    
    ObservableList <String> combo;
    
    private ObservableList <Cafe> cafes;
    
    private ObservableList <Cafe> filtroCafe;
    
    
    @FXML
    public void buscarCafe(KeyEvent event)
    {
    String filtro = txtBuscar.getText().toLowerCase();
    if (filtro.isEmpty())
        {
        this.tb1Cafes.setItems(cafes);
        }
        else
        {
        this.filtroCafe.clear(); 
    for (Cafe c: this.cafes)
      {
        if (c.getTipo().toLowerCase().contains(filtro))
            {
            this.filtroCafe.add(c);
            }
      }
      this.tb1Cafes.setItems(filtroCafe);
      }
    }
    

    @FXML
    void pedirCafe(ActionEvent event) 
    {
        ToggleGroup TG = new ToggleGroup();
        radioCapuccino.setToggleGroup(TG);
        radioLatte.setToggleGroup(TG);
        radioCortado.setToggleGroup(TG);
        RadioButton seleccion = (RadioButton) TG.getSelectedToggle();

    if (seleccion == null) 
    {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("ATENCIÓN");
        alert.setContentText("Debes de completar todos los campos de forma adecuada para realizar el pedido");
        alert.showAndWait();
        System.out.println("Debes seleccionar un tipo de café.");
        return;
    }

    if (ecombo == null) 
    {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("ATENCIÓN");
        alert.setContentText("Debes de completar todos los campos de forma adecuada para realizar el pedido");
        alert.showAndWait();
        System.out.println("Debes seleccionar un tamaño de café.");
        return;
    }

    String cantidadStr = txtCantidad.getText();


    if (!isNumeric(cantidadStr)) 
    {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("ATENCIÓN");
        alert.setContentText("Debes de completar todos los campos de forma adecuada para realizar el pedido");
        alert.showAndWait();
        return;
    }

    int cantidad = Integer.parseInt(cantidadStr);

  
    if (cantidad <= 0) 
    {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("ATENCIÓN");
        alert.setContentText("Debes de completar todos los campos de forma adecuada para realizar el pedido");
        alert.showAndWait();
        return;
    }

    String[] array = saldoActual.getText().split(" ");
    double saldo = Double.parseDouble(array[0]);
    Cafe nuevoCafe = new Cafe(seleccion.getText(), ecombo, cantidad);

    if (saldo < nuevoCafe.getCostoTotal()) 
    {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("AVISO");
        alert.setContentText("No tienes suficiente saldo para realizar el pedido.");
        alert.showAndWait();
        System.out.println("No tienes suficiente saldo para realizar el pedido.");
        return;
    }

    saldo -= nuevoCafe.getCostoTotal();
    saldoActual.setText(Double.toString(saldo) + " " + array[1]);
    txtCantidad.clear();
    System.out.println("Pedido de café exitoso.");

    cafes.add(nuevoCafe);

    tb1Cafes.setItems(cafes);

    txtCantidad.clear();

    TG.getSelectedToggle().setSelected(false);

    cmb1.getSelectionModel().clearSelection();
}

    @FXML
    void elegirColor(ActionEvent event) {
    Color nuevoColor = btnColor.getValue();
    curva.setFill(nuevoColor);
    
}
    @FXML
    void colorDef(ActionEvent event) {
    txtCantidad.clear();
    radioCapuccino.setSelected(false);
    radioLatte.setSelected(false);
    radioCortado.setSelected(false);
    txtRecargar.clear();
    saldoActual.setText("0 €");
    cmb1.getSelectionModel().clearSelection();
    curva.setFill(Color.rgb(120,60,0)); 
    btnColor.setValue(Color.rgb(120,60,0));
}

    private boolean isNumeric(String str) 
    {
    try 
    {
        Integer.parseInt(str);
        return true;
    } catch (NumberFormatException e) 
    {
        return false;
    }
}

@FXML
void recargarDinero(ActionEvent event) 
{
    String recargaStr = txtRecargar.getText();
    
    
    if (!recargaStr.matches("^[0-9]+([.,][0-9]+)?$")) 
    {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("ERROR");
        alert.setContentText("Completa el campo correctamente");
        alert.showAndWait();
        return;
    }
    
    
    recargaStr = recargaStr.replace(',', '.');
    
    if (recargaStr.isEmpty()) 
    {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("INFORMACIÓN");
        alert.setContentText("Debes ingresar una cantidad para recargar dinero.");
        alert.showAndWait();
        return;
    }

    if (Double.parseDouble(recargaStr) <= 0) 
    {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("ERROR");
        alert.setContentText("No puedes ingresar esa cantidad.");
        alert.showAndWait();
        return;
    }

    try 
    {
        double recarga = Double.parseDouble(recargaStr);
        String[] array = saldoActual.getText().split(" ");
        double saldo = Double.parseDouble(array[0]);
        saldo += recarga;
        saldoActual.setText(Double.toString(saldo) + " " + array[1]);
        txtRecargar.clear();
        
    } 
    catch (NumberFormatException e) 
    {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("ERROR");
        alert.setContentText("Debes introducir un número válido.");
        alert.showAndWait();
        System.out.println("Error: Debes introducir un número válido.");
    }
}
        
    @FXML
    void elegirCafe(ActionEvent event) 
    {
        ToggleGroup TG = new ToggleGroup();
        radioCapuccino.setToggleGroup(TG);
        radioLatte.setToggleGroup(TG);
        radioCortado.setToggleGroup(TG);
        RadioButton seleccion = (RadioButton) TG.getSelectedToggle(); 
    }
    
    @FXML
    void eliminarCafe(ActionEvent event) {
    Cafe cafeSeleccionado = tb1Cafes.getSelectionModel().getSelectedItem();

    if (cafeSeleccionado == null) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("ERROR");
        alert.setContentText("Debes seleccionar un café para eliminar.");
        alert.showAndWait();
        System.out.println("Debes seleccionar un café para eliminar.");
        return;
    }

    Alert confirmAlert = new Alert(Alert.AlertType.CONFIRMATION);
    confirmAlert.setTitle("ATENCIÓN");
    confirmAlert.setContentText("¿Estás seguro que deseas eliminar este café?");

    Optional<ButtonType> result = confirmAlert.showAndWait();

    if (result.isPresent() && result.get() == ButtonType.OK) {
        double precioCafe = cafeSeleccionado.getCostoTotal();
        cafes.remove(cafeSeleccionado);
        tb1Cafes.setItems(cafes);

        String[] array = saldoActual.getText().split(" ");
        double saldo = Double.parseDouble(array[0]);
        saldo += precioCafe;
        saldoActual.setText(Double.toString(saldo) + " " + array[1]);

        txtCantidad.clear();

        System.out.println("Café eliminado con éxito.");
    }
}
  
     public void elegirOpcion(ActionEvent event) 
    {
        if(cmb1.getSelectionModel().getSelectedItem()=="Corto: 0.5€")
        {
            System.out.println("Has elegido el programa Corto");
            ecombo="Corto";
        }
        else if(cmb1.getSelectionModel().getSelectedItem()=="Mediano: 1€")
        {
            System.out.println("Has elegido el programa Mediano");
            ecombo="Mediano";
        }
        else if(cmb1.getSelectionModel().getSelectedItem()=="Largo: 2.5€")
        {
            System.out.println("Has elegido el programa Largo");
            ecombo="Largo";
        }
    }
    
    @Override 
    public void initialize(URL location, ResourceBundle resources) 
    { 
      combo=FXCollections.observableArrayList();
      cafes=FXCollections.observableArrayList();
      filtroCafe=FXCollections.observableArrayList();
      combo.addAll("Corto: 0.5€","Mediano: 1€","Largo: 2.5€");
      cmb1.setItems(combo);
      saldoActual.setText("0 €");
      
      columnaTipo.setCellValueFactory(new PropertyValueFactory<>("tipo"));
      columnaTamaño.setCellValueFactory(new PropertyValueFactory<>("tamaño"));
      columnaPrecio.setCellValueFactory(new PropertyValueFactory<>("costoTotal"));
      columnaCantidad.setCellValueFactory(new PropertyValueFactory<>("cantidad"));
    }  
}