package com.mycompany.practicacafeteria;

public class Cafe 
{
  private String tipo, tamaño;  
  private double precio, costoTotal;
  private int cantidad;

    public Cafe(String tipo, String tamaño, int cantidad) 
    {
        this.tipo = tipo;
        this.tamaño = tamaño;
        this.cantidad = cantidad;
        this.precio = obtenerPrecioCafe(tamaño);
        this.costoTotal = precio * cantidad;
    }
    
    private double obtenerPrecioCafe(String tamaño) 
    {
    double precio = 0.0;

        if (tamaño.equals("Corto")) 
        {
            precio = 0.5;
        } 
        else if (tamaño.equals("Mediano")) 
        {
            precio = 1.0;
        } 
        else if (tamaño.equals("Largo")) 
        {
            precio = 2.5;
        }
        return precio;
    }

    public double getCostoTotal() 
    {
        return costoTotal;
    }

    public void setCostoTotal(double costoTotal) 
    {
        this.costoTotal = costoTotal;
    }

    public int getCantidad() 
    {
        return cantidad;
    }

    public void setCantidad(int cantidad) 
    {
        this.cantidad = cantidad;
    }
    
    public String getTipo() 
    {
        return tipo;
    }

    public void setTipo(String tipo) 
    {
        this.tipo = tipo;
    }

    public String getTamaño() 
    {
        return tamaño;
    }

    public void setTamaño(String tamaño) 
    {
        this.tamaño = tamaño;
    }

    public double getPrecio() 
    {
        return precio;
    }

    public void setPrecio(int precio) 
    {
        this.precio = precio;
    }
}
